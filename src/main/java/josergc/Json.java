package josergc;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * <p>Small utility to encode and decode JSON documents</p>
 * <p><a href="http://json.org/">Reference</a></p>
 * @author josergc@gmail.com
 * @since 1.0.0
 */
public class Json {
	/**
	 * Encodes null, {@link Double}, {@link Boolean}, {@link Vector}, and 
	 * {@link Hashtable} objects into a JSON valid document
	 * @param o
	 * @return JSON document
	 */
	public static String encode(Object o) {
		return encode(o,new Hashtable());
	}
	/**
	 * Anti-recursivity implementation
	 * @param o
	 * @param dictionary
	 * @return JSON document
	 */
	private static String encode(Object o, Hashtable dictionary) {
		if (o == null) {
			return "null";
		} else if (o instanceof Double) {
			return o.toString();
		} else if (o instanceof Boolean) {
			return o.toString();
		} else if (o instanceof Vector) {
			Vector v = (Vector)o;
			StringBuffer sb = new StringBuffer('[');
			for (int i = 0, fi = v.size(); i < fi; i++) {
				if (i > 0) {
					sb.append(',');
				}
				Object key = v.elementAt(i);
				Object value = dictionary.get(key);
				if (value == null) {
					dictionary.put(key,value = encode(key,dictionary));
				}
				sb.append(value);
			}
			return sb.append(']').toString();
		} else if (o instanceof Hashtable) {
			Hashtable ht = (Hashtable)o;
			StringBuffer sb = new StringBuffer('{');
			Enumeration keys = ht.keys();
			Enumeration elements = ht.elements();
			boolean addSeparator = false;
			while (keys.hasMoreElements()) {
				if (addSeparator) {
					sb.append(',');
				} else {
					addSeparator = true;
				}
				Object key = keys.nextElement();
				Object encodedKey = dictionary.get(key);
				if (encodedKey == null) {
					dictionary.put(key,encodedKey = encode(key,dictionary));
				}
				Object element = elements.nextElement();
				Object encodedElement = dictionary.get(element);
				if (encodedElement == null) {
					dictionary.put(element,encodedElement = encode(element,dictionary));
				}
				sb.append(encodedKey).append(':').append(encodedElement);
			}
			return sb.append('}').toString();
		} else if (o instanceof String) {
			StringBuffer sb = new StringBuffer('"');
			char[] ca = ((String)o).toCharArray();
			for (int i = 0, fi = ca.length; i < fi; i++) {
				char c = ca[i];
				if (c >= 32 && c <= 126) {
					if (c == '/') {
						sb.append("\\/");
					} else {
						sb.append(c);
					}
				} else {
					switch (c) {
					case '\"': sb.append("\\\""); break;
					case '\\': sb.append("\\\\"); break;
					case '\b': sb.append("\\b"); break;
					case '\f': sb.append("\\f"); break;
					case '\n': sb.append("\\n"); break;
					case '\r': sb.append("\\r"); break;
					case '\t': sb.append("\\t"); break;
					default: sb.append("\\u").append(TextUtils.lfill(Integer.toHexString(c & 0xffff),'0',4));
					}
				}
			}
			return sb.append('"').toString();
		}
		throw new IllegalArgumentException(o.getClass().getName() + " is not supported");
	}
	
	public static Object decode(String document) {
		char[] ca = document.toCharArray();
		int i = skipBlanks(ca,0);
		return decode(ca,i);
	}
	private static Object decode(char[] ca, int i) {
		char c;
		switch(c = ca[i]) {
		case '{': return decodeHashtable(ca,i + 1);
		case '[': return decodeVector(ca,i + 1);
		case '"': return decodeString(ca,i + 1);
		case 't': return decodeTrue(ca,i + 1);
		case 'f': return decodeFalse(ca,i + 1);
		case 'n': return decodeNull(ca,i + 1);
		default: 
			return decodeDouble(ca);
		}
	}
	private static int skipBlanks(char[] ca, int i) {
		int caLength = ca.length;
		while (i < caLength) {
			char c = ca[i];
			if (c == ' ' || c == '\t' || c == '\n') {
				i++;
			} else {
				return i;
			}
		}
		return i;
	}
	
	
}
